FROM openjdk:11-jre-slim

# folders
RUN ["mkdir", "-p", "/opt/app"]
WORKDIR /opt/app

# ports
EXPOSE 8080

# scripts
COPY scripts/entrypoint.sh entrypoint.sh
RUN ["chmod", "+x", "entrypoint.sh"]

ENTRYPOINT [ "sh", "entrypoint.sh" ]

# app executable
ARG JAR_FILE
ADD target/${JAR_FILE} app.jar
