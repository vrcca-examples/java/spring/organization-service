package com.thoughtmechanix.controllers;

import com.thoughtmechanix.model.Organization;
import com.thoughtmechanix.services.OrganizationService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/organizations")
public class OrganizationController {
    private final Logger logger = LoggerFactory.getLogger(OrganizationController.class);

    private final OrganizationService service;

    public OrganizationController(OrganizationService service) {
        this.service = service;
    }

    @RequestMapping(value = "/{organizationId}", method = RequestMethod.GET)
    public Organization retrieve(@PathVariable("organizationId") String organizationId) {
        logRequestsFor("retrieve");
        return service.findById(organizationId);
    }

    @RequestMapping(value = "/{organizationId}", method = RequestMethod.PUT)
    public void update(@PathVariable("organizationId") String orgId, @RequestBody Organization organization) {
        logRequestsFor("update");
        service.update(organization.withId(orgId));
    }

    @RequestMapping(value = "/", method = RequestMethod.POST)
    public Organization save(@RequestBody Organization newOrganization) {
        logRequestsFor("save");
        return retrieve(service.save(newOrganization).getId());
    }

    @GetMapping
    public Iterable<Organization> listAll() {
        logRequestsFor("listAll");
        return service.findAll();
    }

    @DeleteMapping("/{organizationId}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void delete(@PathVariable("organizationId") String orgId) {
        logRequestsFor("delete");
        service.deleteById(orgId);
    }

    private void logRequestsFor(String operation) {
        logger.info("requesting {}", operation);
    }
}
