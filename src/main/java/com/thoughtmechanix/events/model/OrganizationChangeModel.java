package com.thoughtmechanix.events.model;

public class OrganizationChangeModel {
    private final String typeName;
    private final String action;
    private final String organizationId;

    public OrganizationChangeModel(String typeName, String action, String organizationId) {
        this.typeName = typeName;
        this.action = action;
        this.organizationId = organizationId;
    }

    public String getTypeName() {
        return typeName;
    }

    public String getAction() {
        return action;
    }

    public String getOrganizationId() {
        return organizationId;
    }
}
