package com.thoughtmechanix.events;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface CustomChannels {
    @Output("outboundOrgChanges")
    MessageChannel orgChanges();
}
