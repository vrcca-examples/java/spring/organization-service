package com.thoughtmechanix.events.source;

import com.thoughtmechanix.events.CustomChannels;
import com.thoughtmechanix.events.model.OrganizationChangeModel;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.messaging.support.MessageBuilder;

@EnableBinding(CustomChannels.class)
public class SimpleSourceBean {
    private static final Logger logger = LoggerFactory.getLogger(SimpleSourceBean.class);

    private CustomChannels channels;

    public SimpleSourceBean(CustomChannels channels) {
        this.channels = channels;
    }

    public void publishOrgChange(String action, String orgId) {
        logger.info("Sending Kafka message {} for Organization Id {}", action, orgId);

        final var change = new OrganizationChangeModel(
                OrganizationChangeModel.class.getTypeName(),
                action,
                orgId);

        final var message = MessageBuilder
                .withPayload(change)
                .build();
        final boolean sent = channels.orgChanges().send(message);
        logger.info("Kafka message {} {}sent.", action, sent ? "" : "could not be ");
    }
}
