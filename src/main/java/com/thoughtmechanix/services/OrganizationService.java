package com.thoughtmechanix.services;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.thoughtmechanix.events.source.SimpleSourceBean;
import com.thoughtmechanix.model.Organization;
import com.thoughtmechanix.repository.OrganizationRepository;
import org.springframework.stereotype.Service;

@Service
public class OrganizationService {
    public static final String DATABASE_THREAD_POOL = "databaseThreadPool";

    private final OrganizationRepository repository;
    private final SimpleSourceBean simpleSourceBean;

    public OrganizationService(OrganizationRepository repository, SimpleSourceBean simpleSourceBean) {
        this.repository = repository;
        this.simpleSourceBean = simpleSourceBean;
    }

    @HystrixCommand(threadPoolKey = DATABASE_THREAD_POOL)
    public Organization findById(String organizationId) {
        return repository.findById(organizationId).get();
    }

    @HystrixCommand(threadPoolKey = DATABASE_THREAD_POOL)
    public Organization save(Organization newOrganization) {
        final var organization = repository.save(newOrganization.withRandomId());
        simpleSourceBean.publishOrgChange("SAVE", organization.getId());
        return organization;
    }

    @HystrixCommand(threadPoolKey = DATABASE_THREAD_POOL)
    public void update(Organization organization) {
        repository.save(organization);
        simpleSourceBean.publishOrgChange("UPDATE", organization.getId());
    }

    @HystrixCommand(threadPoolKey = DATABASE_THREAD_POOL)
    public void deleteById(String orgId) {
        repository.deleteById(orgId);
        simpleSourceBean.publishOrgChange("DELETE", orgId);
    }

    @HystrixCommand(threadPoolKey = DATABASE_THREAD_POOL)
    public Iterable<Organization> findAll() {
        return repository.findAll();
    }
}