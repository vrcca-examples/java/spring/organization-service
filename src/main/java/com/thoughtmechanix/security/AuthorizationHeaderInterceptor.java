package com.thoughtmechanix.security;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpRequest;
import org.springframework.http.client.ClientHttpRequestExecution;
import org.springframework.http.client.ClientHttpRequestInterceptor;
import org.springframework.http.client.ClientHttpResponse;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.oauth2.provider.authentication.OAuth2AuthenticationDetails;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;

@Component
public class AuthorizationHeaderInterceptor implements ClientHttpRequestInterceptor {

    private final Logger logger = LoggerFactory.getLogger(AuthorizationHeaderInterceptor.class);

    private final RestTemplate restTemplate;

    public AuthorizationHeaderInterceptor(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @PostConstruct
    private void attachInterceptor() {
        final var interceptors = Optional.ofNullable(restTemplate.getInterceptors()).orElseGet(ArrayList::new);
        interceptors.add(this);
        restTemplate.setInterceptors(interceptors);
    }

    @Override
    public ClientHttpResponse intercept(HttpRequest request, byte[] body, ClientHttpRequestExecution execution) throws IOException {
        final String tokenValue = getToken();
        if (tokenValue != null) {
            request.getHeaders().add(HttpHeaders.AUTHORIZATION, "Bearer " + tokenValue);
        }
        return execution.execute(request, body);
    }

    private String getToken() {
        try {
            OAuth2AuthenticationDetails details = (OAuth2AuthenticationDetails)
                    SecurityContextHolder.getContext().getAuthentication().getDetails();
            return details.getTokenValue();
        } catch (Exception e) {
            logger.debug("Could not retrieve token from SecurityContext. Error: {}", e);
            return null;
        }
    }
}
